import string
import random
import re

from django.db.models import Q

from lottery.users.models import Numbers


class ConditionBuilder:
    @staticmethod
    def get_and_condition(conditions: dict) -> Q:
        cond = Q(_connector="AND")
        for k, v in conditions.items():
            if v or isinstance(v, bool) or isinstance(v, list):
                cond.children.append((k, v))
        return cond


def random_SecretKey(randomlength=8, has_lowercase=True, has_uppercase=True, has_number=True, has_special_char=False, custom_words=""):
    """获取随机密码
    @param randomlength: 随机密码长度
    @param has_lowercase: 是否包含小写字母
    @param has_uppercase: 是否包含大写字母
    @param has_number: 是否包含数字
    @param has_special_char: 是否包含特殊字符
    @param custom_words: 加入到随机密码中的自定义词组字符串
    @return: 生成的随机密码"""
    a = list(custom_words)
    if has_lowercase:
        a = a + list(string.ascii_lowercase)
    if has_uppercase:
        a = a + list(string.ascii_uppercase)
    if has_number:
        a = a + list("0123456789")
    if has_special_char:
        a = a + list("+=-@#~,.[]()!%^*$")
    if len(a) < randomlength:
        a = a * (randomlength - len(a))
    random.shuffle(a)
    return ''.join(a[:randomlength])


def check_phone(phone):
    mobile = "^(134|135|136|137|138|139|150|151|152|157|158|159|182|183|184|187|188|147|178|1705)[0-9]+"
    union = "^(130|131|132|155|156|185|186|145|176|1709)[0-9]+"
    telecom = "^(13|153|180|181|189|177|1700)[0-9]+"
    reality = False
    if len(phone) == 11:
        m = re.search(mobile, phone)
        u = re.search(union, phone)
        t = re.search(telecom, phone)
        if m or u or t:
            reality = True
    return reality


def get_num_list(max_num):
    num_9 = max_num -  1
    n = len(str(num_9))
    return [str(num).zfill(n) for num in range(max_num)]


def choice_num(max_num, lottery):
    all_num_list = get_num_list(max_num)
    has_num_list = list(Numbers.objects.filter(record__lottery=lottery).values_list("number", flat=True))
    diff_num_list = [num for num in all_num_list if num not in has_num_list]
    return random.choice(diff_num_list)

