from django.conf import settings
from django.contrib import admin
from django.contrib.auth import admin as auth_admin
from django.contrib.auth import get_user_model
from django.utils.translation import gettext_lazy as _

from django.utils.safestring import mark_safe


from lottery.users.filter import CompanyListFilter, CreatedAtFilter, LotteryListFilter, NameListFilter, PhoneListFilter

from lottery.users.forms import UserAdminChangeForm, UserAdminCreationForm
from lottery.users.models import Lottery, Numbers, Record
from lottery.utils.utils import random_SecretKey

User = get_user_model()


admin.site.site_header = '抽奖管理后台'


@admin.register(User)
class UserAdmin(auth_admin.UserAdmin):
    form = UserAdminChangeForm
    add_form = UserAdminCreationForm
    fieldsets = (
        (None, {"fields": ("username", "password")}),
        (_("Personal info"), {"fields": ("name", "email")}),
        (
            _("Permissions"),
            {
                "fields": (
                    "is_active",
                    "is_staff",
                    "is_superuser",
                    "groups",
                    "user_permissions",
                ),
            },
        ),
        (_("Important dates"), {"fields": ("last_login", "date_joined")}),
    )
    list_display = ["username", "name", "is_superuser"]
    search_fields = ["username"]
    list_filter = ("name", "is_superuser",)

class NumbersInlineAdmin(admin.TabularInline):
    model = Numbers


@admin.register(Lottery)
class LotteryAdmin(admin.ModelAdmin):
    readonly_fields = ("code", "url")
    search_fields = ('title', 'company')
    list_display = ('id', 'code', 'title', 'company', 'start_date', 'end_date', 'description')

    def save_model(self, request, obj, form, change):
        if not change:
            code = random_SecretKey()
            url = settings.URL_PER + code
            obj.code = code
            obj.url = url
        obj.save()


@admin.register(Record)
class RecordAdmin(admin.ModelAdmin):
    list_display = ('id', 'lottery', 'name', 'phone', 'company', 'numbers_info')
    search_fields = ('numbers__number', )
    list_filter = (LotteryListFilter, PhoneListFilter, NameListFilter, CompanyListFilter, ("numbers__created_at", CreatedAtFilter))
    inlines = [
        NumbersInlineAdmin,
    ]
    
    def numbers_info(self, obj):
        num_html = "<p>%s</p>"
        num_str_list = []
        q_set = obj.numbers.all().order_by("id")
        n = 1
        for q in q_set:
            l_time = q.created_at.strftime("%Y-%m-%d %H:%M:%S")
            num_str_list.append(f"第{n}次抽奖,号码段为[<mark>{q.number}</mark>], 时间为[{l_time}]")
            n += 1
        return mark_safe(num_html % "<br/>".join(num_str_list))

    numbers_info.short_description = '抽取的号码段'

@admin.register(Numbers)
class NumbersAdmin(admin.ModelAdmin):
    pass