from django.contrib.auth import get_user_model
from django.utils import timezone
from django.db import transaction

from rest_framework import status
from rest_framework.decorators import action
from rest_framework.mixins import (
    ListModelMixin,
    RetrieveModelMixin,
    UpdateModelMixin,
    CreateModelMixin,
)
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from drf_yasg.utils import swagger_auto_schema

from lottery.users.models import Lottery, Numbers, Record
from lottery.users.api.serializers import (
    LotteryQuerySerializer,
    NumberQuerySerializer,
    RecordCreateSerializer,
    UserSerializer,
    LotterySerializer,
)
from lottery.utils.utils import choice_num

User = get_user_model()


class UserViewSet(RetrieveModelMixin, ListModelMixin, UpdateModelMixin, GenericViewSet):
    serializer_class = UserSerializer
    queryset = User.objects.all()
    lookup_field = "username"

    def get_queryset(self, *args, **kwargs):
        assert isinstance(self.request.user.id, int)
        return self.queryset.filter(id=self.request.user.id)

    @action(detail=False)
    def me(self, request):
        serializer = UserSerializer(request.user, context={"request": request})
        return Response(status=status.HTTP_200_OK, data=serializer.data)


class LottertInfoViewSet(RetrieveModelMixin, GenericViewSet):
    serializer_class = LotterySerializer
    queryset = Lottery.objects.all()
    lookup_field = "code"
    authentication_classes = []
    permission_classes = []

    @swagger_auto_schema(
        operation_summary="抽奖活动详情",
        tags=["lottery"],
        query_serializer=LotteryQuerySerializer,
    )
    def retrieve(self, request, *args, **kwargs):
        print("xxxxxxxxx")
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        return Response(serializer.data)


class RecordViewSet(CreateModelMixin, GenericViewSet):
    serializer_class = LotterySerializer
    queryset = Record.objects.all()
    authentication_classes = []
    permission_classes = []

    @swagger_auto_schema(
        operation_summary="抽奖",
        tags=["lottery"],
        request_body=RecordCreateSerializer,
    )
    def create(self, request, *args, **kwargs):
        serializer = RecordCreateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        record_data = serializer.data
        code = record_data.get("code", "")
        lottery = Lottery.objects.filter(code=code).first()
        if not lottery:
            return Response(
                {"success": False, "message": "此活动已失效"},
                status=status.HTTP_400_BAD_REQUEST,
            )
        if lottery.end_date < timezone.now().date():
            return Response(
                {"success": False, "message": "活动已过期"},
                status=status.HTTP_400_BAD_REQUEST,
            )
        user_num = lottery.user_num
        lotteries_num = lottery.lotteries_num
        sum_num = int(lotteries_num) * int(user_num)
        with transaction.atomic():
            has_num = Numbers.objects.filter(record__lottery=lottery).count()
            if has_num >= sum_num:
                return Response(
                    {"success": False, "message": "抱歉，参与活动人数已满"},
                    status=status.HTTP_400_BAD_REQUEST,
                )
            queryset = self.get_queryset()
            record = queryset.filter(
                lottery=lottery, phone=record_data.get("phone", "")
            ).first()
            if not record:
                record = Record.objects.create(
                    lottery=lottery,
                    phone=record_data.get("phone", ""),
                    name=record_data.get("name", ""),
                    company=record_data.get("company", ""),
                    lotteries_num=1,
                )
            else:
                if record.lotteries_num >= lotteries_num:
                    return Response(
                        {"success": False, "message": "您的抽奖次数已用完"},
                        status=status.HTTP_400_BAD_REQUEST,
                    )
                record.lotteries_num = record.lotteries_num + 1
                record.save()
            number = choice_num(sum_num, lottery)
            while Numbers.objects.filter(
                record__lottery=lottery, number=number
            ).exists():
                number = choice_num(sum_num, lottery)
            Numbers.objects.create(
                number=number,
                record=record,
            )

        return Response(data={"number": number}, status=status.HTTP_201_CREATED)

    @swagger_auto_schema(
        operation_summary="检查剩余次数",
        tags=["lottery"],
        query_serializer=NumberQuerySerializer,
    )
    @action(detail=False, methods=["GET"])
    def check(self, request):
        serializer = NumberQuerySerializer(data=request.GET.dict())
        serializer.is_valid(raise_exception=True)
        record_data = serializer.data
        code = record_data.get("code", "")
        lottery = Lottery.objects.filter(code=code).first()
        if not lottery:
            return Response(
                {"success": False, "message": "此活动已失效"},
                status=status.HTTP_400_BAD_REQUEST,
            )
        if lottery.end_date < timezone.now().date():
            return Response(
                {"success": False, "message": "活动已过期"},
                status=status.HTTP_400_BAD_REQUEST,
            )
        lotteries_num = lottery.lotteries_num
        queryset = self.get_queryset()
        record = queryset.filter(
            lottery=lottery, phone=record_data.get("phone", "")
        ).first()
        if not record:
            num = lotteries_num
        else:
            num = lotteries_num - record.lotteries_num

        return Response(status=status.HTTP_200_OK, data={"num": num})
