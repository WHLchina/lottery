from django.contrib.auth import get_user_model
from rest_framework import serializers

from lottery.users.models import Lottery
from lottery.utils.utils import check_phone

User = get_user_model()


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ["username", "name", "url"]

        extra_kwargs = {
            "url": {"view_name": "api:user-detail", "lookup_field": "username"}
        }


class LotterySerializer(serializers.ModelSerializer):
    class Meta:
        model = Lottery
        fields = ['id', 'code', 'title', 'company', 'start_date', 'end_date', 'description']


class LotteryQuerySerializer(serializers.Serializer):
    code = serializers.CharField(label="活动编号", required=True)


class RecordCreateSerializer(serializers.Serializer):
    code = serializers.CharField(label="活动编号", required=True)
    phone = serializers.CharField(label="手机号", required=True)
    company = serializers.CharField(label="公司名称", required=True)
    name = serializers.CharField(label="姓名", required=True)

    def validate_phone(self, phone):
        # 验证手机号
        # 验证文件大小
        status = check_phone(phone)
        if not status:
            raise serializers.ValidationError('手机号格式不正确')
        
        return phone


class NumberQuerySerializer(serializers.Serializer):
    code = serializers.CharField(label="活动编号", required=True)
    phone = serializers.CharField(label="手机号", required=True)

    def validate_phone(self, phone):
        # 验证手机号
        # 验证文件大小
        status = check_phone(phone)
        if not status:
            raise serializers.ValidationError('手机号格式不正确')
        
        return phone
