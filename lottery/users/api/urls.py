from django.urls import path

from rest_framework_nested import routers

from lottery.users.api.views import LottertInfoViewSet, RecordViewSet

router = routers.SimpleRouter()

app_name = "lottery"

router.register("lottery_info", LottertInfoViewSet)
router.register("record", RecordViewSet)
urlpatterns = router.urls