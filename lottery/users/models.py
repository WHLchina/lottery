from django.db import models

from django.contrib.auth.models import AbstractUser
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

from tinymce.models import HTMLField


class User(AbstractUser):
    """
    Default custom user model for lottery.
    If adding fields that need to be filled at user signup,
    check forms.SignupForm and forms.SocialSignupForms accordingly.
    """

    #: First and last name do not cover name patterns around the globe
    name = models.CharField("昵称", blank=True, max_length=255)
    first_name = None  # type: ignore
    last_name = None  # type: ignore

    def get_absolute_url(self):
        """Get url for user's detail view.

        Returns:
            str: URL for user detail.

        """
        return reverse("users:detail", kwargs={"username": self.username})

    class Meta:
        verbose_name = "后台账号管理"
        verbose_name_plural = "后台账号管理"



# 参考 https://github.com/gothinkster/django-realworld-example-app/blob/master/conduit/apps/core/models.py
class TimestampedModel(models.Model):
    # A timestamp representing when this object was created.
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='创建时间')

    # A timestamp reprensenting when this object was last updated.
    updated_at = models.DateTimeField(auto_now=True, verbose_name='保存时间')

    class Meta:
        abstract = True
        # 一般使用逆序
        ordering = ['-created_at', '-updated_at']


class Lottery(TimestampedModel):
    code = models.CharField(max_length=200, null=True, blank=True, verbose_name="随机生成", help_text="自动生成")
    title = models.CharField(max_length=255, verbose_name="活动名称")
    company = models.CharField(max_length=200, null=True, blank=True, verbose_name="公司名称")
    start_date = models.DateField("抽奖开始时间")
    end_date = models.DateField("抽奖结束时间")
    user_num = models.IntegerField(default=0, verbose_name="参与抽奖人数")
    lotteries_num = models.IntegerField(default=2, verbose_name="每个人抽奖次数")
    description = HTMLField(null=True, blank=True, verbose_name="活动规则说明")
    url = models.TextField(null=True, blank=True, verbose_name="活动链接", help_text="自动生成")

    class Meta:
        verbose_name_plural = '抽奖活动列表'
        verbose_name = '抽奖活动列表'

    def __str__(self) -> str:
        return self.title


class Record(TimestampedModel):
    
    lottery = models.ForeignKey(
        Lottery,
        null=True,
        related_name="records",
        on_delete=models.SET_NULL,
        db_constraint=False,
        verbose_name="所属抽奖活动"
    )
    phone = models.CharField(max_length=11, verbose_name="手机号")
    name = models.CharField(max_length=200, verbose_name="姓名")
    company = models.CharField(max_length=200, verbose_name="公司名称")
    lotteries_num = models.IntegerField(default=0, verbose_name="抽奖次数")
    # numbers = models.JSONField(null=True, blank=True, verbose_name="号码段", default=list)
    # lottery_times = models.JSONField(null=True, blank=True, verbose_name="抽奖时间", default=list)

    class Meta:
        verbose_name_plural = '用户参与抽奖记录'
        verbose_name = '用户参与抽奖记录'


class Numbers(models.Model):
    
    record = models.ForeignKey(
        Record,
        null=True,
        related_name="numbers",
        on_delete=models.SET_NULL,
        db_constraint=False,
        verbose_name="抽奖记录"
    )
    number = models.CharField(max_length=11, verbose_name="号码段")
    # A timestamp representing when this object was created.
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='抽奖时间')

    # A timestamp reprensenting when this object was last updated.
    updated_at = models.DateTimeField(auto_now=True, verbose_name='保存时间')
    class Meta:
        verbose_name_plural = '号码段记录'
        verbose_name = '号码段记录'
