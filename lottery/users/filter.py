from django.contrib.admin import ListFilter, SimpleListFilter, DateFieldListFilter
from django.core.exceptions import ImproperlyConfigured
from django.utils.translation import gettext_lazy as _

from lottery.users.models import Record
from lottery.utils.utils import ConditionBuilder

class SingleTextInputFilter(ListFilter):
    """
    renders filter form with text input and submit button
    """
    parameter_name = None
    # template = "textinput_filter.html"

    def __init__(self, request, params, model, model_admin):
        super(SingleTextInputFilter, self).__init__(
            request, params, model, model_admin)
        if self.parameter_name is None:
            raise ImproperlyConfigured(
                "The list filter '%s' does not specify "
                "a 'parameter_name'." % self.__class__.__name__)

        if self.parameter_name in params:
            value = params.pop(self.parameter_name)
            self.used_parameters[self.parameter_name] = value

    def value(self):
        """
        Returns the value (in string format) provided in the request's
        query string for this filter, if any. If the value wasn't provided then
        returns None.
        """
        return self.used_parameters.get(self.parameter_name, None)

    def has_output(self):
        return True

    def expected_parameters(self):
        """
        Returns the list of parameter names that are expected from the
        request's query string and that will be used by this filter.
        """
        return [self.parameter_name]


    def choices(self, cl):
        all_choice = {
            'selected': self.value() is None,
            'query_string': cl.get_query_string({}, [self.parameter_name]),
            'display': _('All'),
        }
        return ({
            'get_query': cl.params,
            'current_value': self.value(),
            'all_choice': all_choice,
            'parameter_name': self.parameter_name
        }, )


class CreatedAtFilter(DateFieldListFilter):

    title = '抽奖时间'
    parameter_name = 'created_at'

    # def __init__(self, *args, **kwargs):
    #     super(CreatedAtFilter, self).__init__(*args, **kwargs)

    #     now = timezone.now()
    #     # When time zone support is enabled, convert "now" to the user's time
    #     # zone so Django's definition of "Today" matches what the user expects.
    #     if timezone.is_aware(now):
    #         now = timezone.localtime(now)

    #     today = now.date()

    #     self.links += ((
    #         (_('Next 7 days'), {
    #             self.lookup_kwarg_since: str(today),
    #             self.lookup_kwarg_until: str(today + datetime.timedelta(days=7)),
    #         }),
    #     ))
    def queryset(self, request, queryset):
        try:
            if not self.used_parameters:
                return queryset
            return queryset.filter(**self.used_parameters)
        except Exception as e:
            # Fields may raise a ValueError or ValidationError when converting
            # the parameters to the correct type.
            raise e


class MyFilter(SimpleListFilter):

    title = ''
    parameter_name = ''

    def lookups(self, request, model_admin):
        request_data = request.GET.dict()
        queryset = Record.objects.all()
        conditions = {}
        for k, v in request_data.items():
            if k == "q":
                conditions["numbers__number__icontains"] = v
            elif k == self.parameter_name or k.count("created_at"):
                continue
            else:
                filter_by = '{0}__{1}'.format(k, 'icontains')
                conditions[filter_by] = v
                
        cond = ConditionBuilder.get_and_condition(conditions)
        queryset = queryset.filter(cond)
        # if q:
        #     queryset = queryset.filter(lottery__title__icontains=q)
        queryset = queryset.values(self.parameter_name).distinct()
        for obj in queryset:
            v = obj.get(self.parameter_name, "")
            yield (v, v)

    def queryset(self, request, queryset):
        if self.value():
            filter_by = '{0}__{1}'.format(self.parameter_name, 'icontains')
            filter_data = {filter_by: self.value()}
            return queryset.filter(**filter_data)


class LotteryListFilter(MyFilter):
    title = '活动名称'
    parameter_name = 'lottery__title'


class PhoneListFilter(MyFilter):
    title = '手机号'
    parameter_name = 'phone'


class NameListFilter(MyFilter):
    title = '姓名'
    parameter_name = 'name'


class CompanyListFilter(MyFilter):
    title = '公司名称'
    parameter_name = 'company'
